import { Component } from '@angular/core';

@Component({
    selector: 'app-body',
    templateUrl: './body.component.html',
    styleUrls: ['./body.component.scss']
})

export class BodyComponent {
    mostrar = true;
    frase: any = {
        mensaje: 'PURAS WEAS',
        autor: 'Quien Sabe'
    };
    public lista: any[] = ['hola', 0, true, 'mañana', undefined];
}
